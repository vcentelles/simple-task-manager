# Simple Task Manager
This is a simple task manager for a technical challenge for a interview.

### Introduction
The task manager has the next properties:

* "Sign in" and "Log in" for users.
* Sending emails using the system emails from AppEngine.
* Use the intermediate AppEngine cache (for a simple task).
* It uses the NoSQL BD of AppEngine.
* It manages the user tasks (0 to N for user/ One list per user).
* It can order all columns.
* It can filter for task state (Pending, expired or completed)



