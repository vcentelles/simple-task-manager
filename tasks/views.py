from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.contrib import messages
#from django.core.cache import cache
from google.appengine.api import memcache as cache
# In the new versios of djangoappengine, we should use the django.cache
from django.core.context_processors import csrf
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.db.utils import ConnectionDoesNotExist
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import Context
from tasks.models import Task
from tasks.forms import NewTaskForm

__author__ = 'victor'


@login_required
def show_tasks(request):
    """
    Show a list with all the tasks of the logged user.
    You can send for POST a new task, or send by GET filters for list.
    """

    errors = ""
    form = NewTaskForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            obj = form.save(commit=False)
            obj.user = request.user
            obj.save()

            cache.set(str(obj.id), obj)
        else:
            errors = form.errors

    form = NewTaskForm(initial={"state": 0})
    task_list = Task.objects.filter(user_id=request.user.id, deleted=False).order_by("-date")

    #Update expired tasks
    task_list.filter(state=0, date__lt=datetime.now()).update(state=1)

    if request.method == "GET":
        filter = request.GET.get("filter", None)
        if filter == "0":
            task_list = task_list.filter(state=0)
        elif filter == "1":
            task_list = task_list.filter(state=1)
        elif filter == "2":
            task_list = task_list.filter(state=2)

    context = Context({"errors": errors,
                       "new_task_form": form,
                       "now": datetime.now,
                       "task_list": task_list,
                       "user": request.user})
    context.update(csrf(request))

    return render_to_response("tasks/list.html", context)


## Completed task #############################################################
@login_required
def complete_task(request, task_id):
    """
    Change the state of a task to "complete" state (state: 2)

    @param task_id: the id of the task that the user wants to change.
    """
    task = cache.get(task_id)
    if not task:
        task = Task.objects.get(id=task_id)
    if request.user == task.user:
        task.state = 2
        task.save()
        cache.set(task_id, task)
    else:
        #Someone tries to delete a task without permission
        pass

    return HttpResponseRedirect(reverse("task_list"))


## Edit task ##################################################################
@login_required
def edit_task(request, task_id):
    """
    Edit the attributes of a task.

    @param task_id: the id of the task that the user wants to change.
    """
    try:
        task = cache.get(task_id)
        if not task:
            task = Task.objects.get(id=task_id)
    except ObjectDoesNotExist:
        #TODO: I should check more exception (ValueError, etc.)
        messages.add_message(request,
                             messages.ERROR,
                             "The task doesn't exist")
        return HttpResponseRedirect(reverse("task_list"))

    if request.user == task.user:
        errors = ""

        if request.method == "POST":
            form = NewTaskForm(request.POST)
            if form.is_valid():
                task.title = form.cleaned_data["title"]
                task.description = form.cleaned_data["description"]
                task.date = form.cleaned_data["date"]
                task.state = form.cleaned_data["state"]
                task.save()

                cache.set(task_id, task)
                #The form.save doesn't work good

                #obj = form.save(commit=False)
                #obj.id = task_id
                #obj.user = request.user
                #obj.save()
                return HttpResponseRedirect(reverse("task_list"))
            else:
                errors = form.errors

        form = NewTaskForm(instance=task, initial={"state": 0})
        context = Context({"errors": errors,
                           "edit_task_form": form,
                           "task": task})
        context.update(csrf(request))
        return render_to_response("tasks/edit.html", context)

    else:
        return HttpResponseRedirect(reverse("task_list"))


## Delete task ################################################################
@login_required
def delete_task(request, task_id):
    """
    Delete a task of the user task list. If the task doesn't exist we pass
    a error message to Django for a future processing (TODO).

    @params task_id the if of the task that the user wants delete pass
.
    """
    try:
        task = cache.get(task_id)
        if not task:
            task = Task.objects.get(id=task_id)
        if request.user == task.user:
            task.deleted = True
            task.save()
            cache.set(task_id, task)
        else:
            #TODO: show the django.log.messages in the web page (ie: with bootstrap-toolkit)
            #Someone tries to delete a task without permission
            messages.add_message(request,
                                 messages.ERROR,
                                 "You don't have permission to access")
    except ObjectDoesNotExist:
        #TODO: I should check more exception (ValueError, etc.)
        #The task doesn't exist
        messages.add_message(request, messages.ERROR, "Task doesn't exist")

    return show_tasks(request)


## Delete all tasks ###########################################################
@login_required
def delete_all_tasks(request):
    """
    Delete all tasks of the user. For time limitations, we don't ask confirmation.
    """
    #tasks = Task.objects.filter(user=request.user).delete()
    tasks = Task.objects.filter(user=request.user).update(deleted=True)
    return HttpResponseRedirect(reverse("task_list"))
