from django.forms import ModelForm
from tasks.models import Task

__author__ = 'victor'

class NewTaskForm(ModelForm):
    class Meta:
        model = Task
        fields = ("title", "description", "date", "state")
