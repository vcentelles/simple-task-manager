from django.db import models
from django.contrib.auth.models import User

TASK_STATES = (
    (0, "Pending"),
    (1, "Expired"),
    (2, "Completed")
)

class Task(models.Model):
    user = models.OneToOneField(User)
    title = models.CharField(max_length=80)
    date = models.DateTimeField()
    description = models.TextField()
    state = models.IntegerField(choices=TASK_STATES)
    deleted = models.BooleanField(default=False)

