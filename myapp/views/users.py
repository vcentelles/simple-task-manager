from django.template.loader import get_template
from django.contrib.auth.views import logout
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import AuthenticationForm
from django.core.context_processors import csrf
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import Context
from myapp.forms.creation_form import UserCreateForm
from google.appengine.api import mail
from myapp.settings import SEND_EMAIL

__author__ = 'victor'


## Login ######################################################################

def login_action(request):
    """Log a user """
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return HttpResponseRedirect(reverse("task_list"))
        else:
            return HttpResponseRedirect("<h1>Disable account</h1>")
    else:
        return HttpResponseRedirect("<h1>Invalid login</h1>")


def login_form(request):
    """Manage the login page and the login process"""
    if request.method == "POST":
        return login_action(request)
    else:
        #login_form = UserForm()
        form = AuthenticationForm()
        context = Context({"login_form": form})
        context.update(csrf(request))
        return render_to_response("users/login.html", context)


## Log out ###################################################################

def logout_view(request):
    """Logout a user and redirect to login page"""
    return logout(request, reverse("login"))


## Sign in ####################################################################

def send_signin_emails(username, email):
    """
    If settings.SEND_EMAIL is active, it sends the emails for a registry
    process (for time limitation, I don't send email confirmation with
    hash link, etc.)
    """
    if SEND_EMAIL:
        temp = get_template("emails/new_user_log.txt")
        context = Context({'username': username, 'email': email})
        body = temp.render(context)
        mail.send_mail(sender="victor.centelles@gmail.com",
                       to="victor.centelles@gmail.com",
                       subject="Nuevo registro",
                       body=body)

        temp = get_template("emails/new_user.txt")
        body = temp.render(context)
        mail.send_mail(sender="victor.centelles@gmail.com",
                       to=email,
                       subject="Gracias por registrarte",
                       body=body)


def signin_form(request):
    """
    Manage the sigin process (and the sigin django form).
    """
    error = ""
    if request.method == "POST":
        #form = UserCreationForm(request.POST)
        form = UserCreateForm(request.POST)
        if form.is_valid():
            form.save()
            send_signin_emails(form.cleaned_data['username'], form.cleaned_data["email"])
            return HttpResponseRedirect(reverse("login"))
        else:
            error = form.errors

    form = UserCreateForm()
    context = Context({"signin_form": form, "error": error})
    context.update(csrf(request))
    return render_to_response("users/signin.html", context)

