from django.conf.urls import patterns, url
from django.http import HttpResponse

__author__ = 'victor'

#def test(request):
#    return HttpResponse("<h2>Users...</h2>")

urlpatterns = patterns('',
#    url(r'^$', test),
    url(r'^login/', "myapp.views.users.login_form", name="login"),
    url(r'^logout', "myapp.views.users.logout_view", name="logout"),
    url(r'^signin/', "myapp.views.users.signin_form", name="signin"),
)

