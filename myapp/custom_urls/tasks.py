from django.conf.urls import patterns, url
from django.http import HttpResponse

__author__ = 'victor'

#def test(request):
#    return HttpResponse("<h2>Mmm...</h2>")

urlpatterns = patterns('',
    #url(r'^$', test),
    url(r'^list/', "tasks.views.show_tasks", name="task_list"),
    url(r'^complete/(?P<task_id>\d+)/', "tasks.views.complete_task", name="complete_task"),
    url(r'^edit/(?P<task_id>\d+)/', "tasks.views.edit_task", name="edit_task"),
    url(r'^delete/(?P<task_id>\d+)/', "tasks.views.delete_task", name="delete_task"),
    url(r'^delete_all/', "tasks.views.delete_all_tasks", name="delete_all_tasks"),
)