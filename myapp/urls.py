from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from django.shortcuts import render_to_response
from django.template import Context


def home(request):
    context = Context()
    return render_to_response("home.html", context)


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'web.views.home', name='home'),
    # url(r'^web/', include('web.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
#    url(r'test/', test),
    url(r'^/?$', home),
    url(r'users/', include("myapp.custom_urls.users")),
    url(r'tasks/', include("myapp.custom_urls.tasks")),
)